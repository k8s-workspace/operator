package controllers

import (
	"context"
	"encoding/json"
	"fmt"

	apierrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/types"
	ref "k8s.io/client-go/tools/reference"
	"sigs.k8s.io/controller-runtime/pkg/client"

	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"

	workspacev1 "gitlab.com/bailinhe/k8s-workspace/api/v1"
	workspacedefaults "gitlab.com/bailinhe/k8s-workspace/controllers/workspace-defaults"
)

// findContainerWithName finds the container with the given name from a list
// of containers, returns nil if not found, returns pointer to the container if
// found
func findContainerWithName(
	containers []corev1.Container,
	name string,
) (*corev1.Container, int) {

	index := -1
	for i, cont := range containers {
		if cont.Name == name {
			index = i
			break
		}
	}

	if index == -1 {
		return nil, index
	}

	return &containers[index], index
}

// findVolumeWithName finds the volume with the given name from a list
// of volumes, returns nil if not found, returns pointer to the volume if
// found
func findVolumeWithName(
	volumes []corev1.Volume,
	name string,
) (*corev1.Volume, int) {

	index := -1
	for i, vol := range volumes {
		if vol.Name == name {
			index = i
			break
		}
	}

	if index == -1 {
		return nil, index
	}

	return &volumes[index], index
}

// mapContainerMountsWithContainerName maps all container mounts from a list
// of sharevolumes to their container names
func mapContainerMountsWithContainerName(
	volumes []workspacev1.SharedVolume,
) map[string][]corev1.VolumeMount {

	resp := map[string][]corev1.VolumeMount{}

	for _, vol := range volumes {
		for _, containerMount := range vol.ContainerMounts {
			resp[containerMount.Container] = append(
				resp[containerMount.Container],
				corev1.VolumeMount{
					MountPath: containerMount.Path,
					Name:      fmt.Sprintf("supportservice-%s", vol.Name),
				},
			)
		}
	}

	return resp
}

func (r *SupportServiceReconciler) buildDeploymentPatch(
	deployment *appsv1.Deployment,
	onDelete bool,
) {
	// imagePullSecrets
	for _, imagePullSecret := range r.resDef.Spec.ImagePullSecrets {
		index := -1
		for i, ips := range deployment.Spec.Template.Spec.ImagePullSecrets {
			if ips.Name == imagePullSecret.Name {
				index = i
				break
			}
		}

		if !onDelete {
			// not on delete
			if index == -1 {
				// append secret if not found
				deployment.Spec.Template.Spec.ImagePullSecrets = append(
					deployment.Spec.Template.Spec.ImagePullSecrets,
					imagePullSecret,
				)
			} else {
				// replace if found
				deployment.Spec.Template.Spec.ImagePullSecrets[index] = imagePullSecret
			}

		} else {
			// on delete
			if index != -1 {
				// remove on delete
				deployment.Spec.Template.Spec.ImagePullSecrets = append(
					deployment.Spec.Template.Spec.ImagePullSecrets[:index],
					deployment.Spec.Template.Spec.ImagePullSecrets[index+1:]...,
				)
			}
		}
	}

	volumeMountsWithContainerName := mapContainerMountsWithContainerName(
		r.resDef.Spec.SharedVolumes,
	)

	// volumes
	if r.resDef.Spec.SharedVolumes != nil {
		for _, sharedvolume := range r.resDef.Spec.SharedVolumes {
			name := fmt.Sprintf("supportservice-%s", sharedvolume.Name)
			found, index := findVolumeWithName(
				deployment.Spec.Template.Spec.Volumes,
				name,
			)

			if !onDelete {
				vol := corev1.Volume{
					Name: name,
					VolumeSource: corev1.VolumeSource{
						PersistentVolumeClaim: &corev1.PersistentVolumeClaimVolumeSource{
							ClaimName: name,
						},
					},
				}

				if found == nil {
					deployment.Spec.Template.Spec.Volumes = append(
						deployment.Spec.Template.Spec.Volumes,
						vol,
					)
				} else {
					*found = vol
				}

			} else {
				if found != nil {
					deployment.Spec.Template.Spec.Volumes = append(
						deployment.Spec.Template.Spec.Volumes[:index],
						deployment.Spec.Template.Spec.Volumes[index+1:]...,
					)
				}
			}
		}

	}

	// containers
	for _, cont := range r.resDef.Spec.Containers {
		found, index := findContainerWithName(
			deployment.Spec.Template.Spec.Containers,
			cont.Spec.Name,
		)

		// on create / update
		if !onDelete {
			contSpec := cont.Spec.DeepCopy()

			// helper command
			if cont.UseHelperCmdPreventingCrashLoop {
				contSpec.Command = workspacedefaults.HelperCmdPreventingCrashLoop.Command
				contSpec.Args = workspacedefaults.HelperCmdPreventingCrashLoop.Args
			}

			// volumemounts
			containerMounts := volumeMountsWithContainerName[contSpec.Name]

			for _, containerMount := range containerMounts {
				index := -1
				for i, vm := range contSpec.VolumeMounts {
					if vm.Name == containerMount.Name {
						index = i
						break
					}
				}

				if index == -1 {
					contSpec.VolumeMounts = append(
						contSpec.VolumeMounts,
						containerMount,
					)
				} else {
					contSpec.VolumeMounts[index] = containerMount
				}
			}

			homeVolumeMount := corev1.VolumeMount{
				Name:      "home",
				MountPath: r.resDef.Spec.HomeVolumeMount.MountPath,
			}

			if r.resDef.Spec.HomeVolumeMount.SubPath != "" {
				homeVolumeMount.SubPath = r.resDef.Spec.HomeVolumeMount.SubPath
			}

			contSpec.VolumeMounts = append(
				contSpec.VolumeMounts,
				homeVolumeMount,
			)

			if found == nil {
				deployment.Spec.Template.Spec.Containers = append(
					deployment.Spec.Template.Spec.Containers,
					*contSpec,
				)
			} else {
				*found = *contSpec
			}

		} else {
			// on delete
			if found != nil {
				deployment.Spec.Template.Spec.Containers = append(
					deployment.Spec.Template.Spec.Containers[:index],
					deployment.Spec.Template.Spec.Containers[index+1:]...,
				)
			}
		}

	}

	// workspace container
	workspaceContainer, _ := findContainerWithName(
		deployment.Spec.Template.Spec.Containers,
		"workspace",
	)

	if workspaceContainer == nil {
		return
	}

	// add shared volume mount to container if not exists
	for _, sharedvolume := range r.resDef.Spec.SharedVolumes {
		if sharedvolume.WorkspacePath == "" {
			continue
		}

		name := fmt.Sprintf("supportservice-%s", sharedvolume.Name)
		index := -1
		for i, vm := range workspaceContainer.VolumeMounts {
			if vm.Name == name {
				index = i
				break
			}
		}

		if !onDelete {
			if index == -1 {
				// append if not found
				workspaceContainer.VolumeMounts = append(
					workspaceContainer.VolumeMounts,
					corev1.VolumeMount{
						Name:      name,
						MountPath: sharedvolume.WorkspacePath,
					},
				)
			} else {
				// update if found
				workspaceContainer.VolumeMounts[index] = corev1.VolumeMount{
					Name:      name,
					MountPath: sharedvolume.WorkspacePath,
				}
			}

		} else {
			// remove volume mount on delete
			if index != -1 {
				workspaceContainer.VolumeMounts = append(
					workspaceContainer.VolumeMounts[:index],
					workspaceContainer.VolumeMounts[index+1:]...,
				)
			}
		}
	}

}

func (r *SupportServiceReconciler) updateWorkspaceDeployment(
	onDelete bool,
) error {
	ctx := context.Background()

	// get existing deployment
	deployment := &appsv1.Deployment{}
	err := r.Client.Get(
		ctx,
		client.ObjectKey{
			Namespace: r.resDef.Namespace,
			Name:      r.workspace.Status.Deployment.Name,
		},
		deployment,
	)

	if apierrors.IsNotFound(err) {
		r.log.Error(err, "Workspace deployment is not found")
		return err

	} else if err != nil {
		r.log.Error(err, "Failed to gather deployment info")
		return err

	}

	// update if found
	r.log.Info("Updating existing deployment")

	r.buildDeploymentPatch(deployment, onDelete)

	patch, jsonErr := json.Marshal(deployment)
	if jsonErr != nil {
		r.log.Error(jsonErr, "Failed to create deployment update patch")
		return jsonErr
	}

	if err := r.Client.Patch(
		ctx,
		deployment,
		client.RawPatch(types.MergePatchType, patch),
	); err != nil {
		r.log.Error(err, "Failed to update deployment")
		return err
	}

	if !onDelete {
		deploymentRef, err := ref.GetReference(r.Scheme, deployment)
		if err != nil {
			return err
		}

		r.resDef.Status.AttachedTo = *deploymentRef
	}

	return nil
}
