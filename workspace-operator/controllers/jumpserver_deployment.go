package controllers

import (
	"context"
	"fmt"

	ref "k8s.io/client-go/tools/reference"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"

	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	workspacedefaults "gitlab.com/bailinhe/k8s-workspace/controllers/workspace-defaults"
)

func (r *JumpServerReconciler) buildDeployment() *appsv1.Deployment {
	labels := map[string]string{
		"app": "workspace-jumpserver",
	}

	volumes := []corev1.Volume{
		{
			Name: "dotssh",
			VolumeSource: corev1.VolumeSource{
				EmptyDir: &corev1.EmptyDirVolumeSource{},
			},
		},
		{
			Name: "authorized-keys",
			VolumeSource: corev1.VolumeSource{
				Secret: &corev1.SecretVolumeSource{
					SecretName: "jumpserver-authorized-keys",
				},
			},
		},
		{
			Name: "ssh-jump-server-config",
			VolumeSource: corev1.VolumeSource{
				PersistentVolumeClaim: &corev1.PersistentVolumeClaimVolumeSource{
					ClaimName: "ssh-jump-server-config",
				},
			},
		},
		{
			Name: "sshd-config",
			VolumeSource: corev1.VolumeSource{
				ConfigMap: &corev1.ConfigMapVolumeSource{
					LocalObjectReference: corev1.LocalObjectReference{
						Name: "jump-server-sshd-config",
					},
				},
			},
		},
	}

	containers := []corev1.Container{
		{
			Image:           workspacedefaults.JumpServerImg,
			ImagePullPolicy: "IfNotPresent",
			Name:            "open-ssh-server",
			Resources:       r.resDef.Spec.SSHServer.Resources,
			Ports: []corev1.ContainerPort{{
				ContainerPort: 22,
				Name:          "ssh",
			}},
			VolumeMounts: []corev1.VolumeMount{
				{
					Name:      "dotssh",
					MountPath: "/home/jump/.ssh",
				},
				{
					Name:      "ssh-jump-server-config",
					MountPath: "/etc/ssh",
				},
			},
		},
		{
			Image:           workspacedefaults.ConfigReloadImage,
			ImagePullPolicy: "IfNotPresent",
			Name:            "config-reload",
			Resources:       r.resDef.Spec.AuthorizedKeysReload.Resources,
			VolumeMounts: []corev1.VolumeMount{
				{
					Name:      "dotssh",
					MountPath: "/.ssh",
				},
				{
					Name:      "authorized-keys",
					MountPath: "/auth-keys-secrets",
				},
				{
					Name:      "ssh-jump-server-config",
					MountPath: "/ssh",
				},
				{
					Name:      "sshd-config",
					MountPath: "/sshd-config",
				},
			},
			SecurityContext: &corev1.SecurityContext{
				Capabilities: &corev1.Capabilities{
					Add: []corev1.Capability{
						"SYS_PTRACE",
					},
				},
			},
		},
	}

	shareProcessNamespace := true

	deployment := &appsv1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name:      fmt.Sprintf("%s-jumpserver", r.resDef.Name),
			Namespace: r.resDef.Namespace,
			Labels:    labels,
		},

		Spec: appsv1.DeploymentSpec{
			Replicas: &r.resDef.Spec.Size,
			Selector: &metav1.LabelSelector{
				MatchLabels: labels,
			},

			Template: corev1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Labels: labels,
				},

				Spec: corev1.PodSpec{
					Volumes:               volumes,
					Containers:            containers,
					ShareProcessNamespace: &shareProcessNamespace,
				},
			},
		},
	}

	return deployment
}

func (r *JumpServerReconciler) deployment() error {
	ctx := context.Background()

	// build deployment
	deploymentDef := r.buildDeployment()
	// add ownership
	if err := controllerutil.SetControllerReference(
		r.resDef,
		deploymentDef,
		r.Scheme,
	); err != nil {
		return err
	}

	// get existing deployment
	deployment := appsv1.Deployment{}
	err := r.Client.Get(
		ctx,
		client.ObjectKey{
			Namespace: deploymentDef.GetObjectMeta().GetNamespace(),
			Name:      deploymentDef.GetObjectMeta().GetName(),
		},
		&deployment,
	)

	// deploy if not found
	if apierrors.IsNotFound(err) {
		r.log.Info("Deployment is not found, creating one")

		if err := r.Client.Create(ctx, deploymentDef); err != nil {
			r.log.Error(err, "Failed to create deployment")

			return err
		}

	} else if err != nil {
		r.log.Error(err, "Failed to gather deployment info")
		return err

		// update if found
	} else {
		r.log.Info("Updating existing deployment")

		if err := r.Client.Update(ctx, deploymentDef); err != nil {
			r.log.Error(err, "Failed to update deployment")
			return err
		}
	}

	deploymentRef, err := ref.GetReference(r.Scheme, deploymentDef)

	if err != nil {
		return err
	}

	r.resDef.Status.Deployment = *deploymentRef

	return nil
}
