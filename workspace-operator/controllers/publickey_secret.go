package controllers

import (
	"context"
	"fmt"

	ref "k8s.io/client-go/tools/reference"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"

	corev1 "k8s.io/api/core/v1"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func (r *PublicKeyReconciler) buildPublicKeySecret() *corev1.Secret {
	labels := map[string]string{
		"k8sWorkspaceSecretType": "publickey",
	}

	data := map[string][]byte{
		"id_rsa_pub": []byte(r.resDef.Spec.KeyData),
	}

	secret := &corev1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Name:      fmt.Sprintf("%s-publickey", r.resDef.Name),
			Namespace: r.resDef.Namespace,
			Labels:    labels,
		},

		Type: corev1.SecretTypeOpaque,
		Data: data,
	}

	return secret
}

func (r *PublicKeyReconciler) createPublicKey() error {
	ctx := context.Background()

	// build secret
	secretDef := r.buildPublicKeySecret()
	// add ownership
	if err := controllerutil.SetControllerReference(
		r.resDef,
		secretDef,
		r.Scheme,
	); err != nil {
		return err
	}

	// get existing secret
	secret := &corev1.Secret{}
	err := r.Client.Get(
		ctx,
		client.ObjectKey{
			Namespace: secretDef.GetObjectMeta().GetNamespace(),
			Name:      secretDef.GetObjectMeta().GetName(),
		},
		secret,
	)

	// deploy if not found
	if apierrors.IsNotFound(err) {
		r.log.Info("PublicKey secret is not found, creating one")
		if err := r.Client.Create(ctx, secretDef); err != nil {
			r.log.Error(err, "Failed to create publickey secret")
			return err
		}

	} else if err != nil {
		r.log.Error(err, "Failed to gather publickey secret info")
		return err

	} else if string(secret.Data["id_rsa_pub"]) != string(secretDef.Data["id_rsa_pub"]) {
		r.log.Info("Update existing publicKey secret")

		if err := r.Client.Update(ctx, secretDef); err != nil {
			r.log.Error(err, "Failed to create publickey secret")
			return err
		}
	}

	secretRef, err := ref.GetReference(r.Scheme, secretDef)

	if err != nil {
		return err
	}

	r.resDef.Status.AssociatedSecret = *secretRef

	return nil
}
