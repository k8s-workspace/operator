package controllers

import (
	"context"
	"fmt"

	workspacedefaults "gitlab.com/bailinhe/k8s-workspace/controllers/workspace-defaults"

	apierrors "k8s.io/apimachinery/pkg/api/errors"
	ref "k8s.io/client-go/tools/reference"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func (r *WorkspaceReconciler) buildSSHdConfigMap() *corev1.ConfigMap {
	labels := map[string]string{
		"app": "workspace",
	}

	sshdConfig := r.resDef.Spec.SSHdConfig

	if sshdConfig == "" {
		sshdConfig = workspacedefaults.SSHdConfig
	}

	cm := &corev1.ConfigMap{
		ObjectMeta: metav1.ObjectMeta{
			Name:      fmt.Sprintf("%s-workspace-sshd-config", r.resDef.Name),
			Namespace: r.resDef.Namespace,
			Labels:    labels,
		},

		Data: map[string]string{
			"sshd_config": sshdConfig,
		},
	}

	return cm
}

func (r *WorkspaceReconciler) createSSHdConfigMap() error {
	ctx := context.Background()

	// build configmap
	cmDef := r.buildSSHdConfigMap()

	// add ownership
	if err := controllerutil.SetControllerReference(
		r.resDef,
		cmDef,
		r.Scheme,
	); err != nil {
		return err
	}

	// get existing configmap
	cm := &corev1.ConfigMap{}
	err := r.Client.Get(
		ctx,
		client.ObjectKey{
			Name:      cmDef.Name,
			Namespace: cmDef.Namespace,
		},
		cm,
	)

	// create configmap if not found
	if apierrors.IsNotFound(err) {
		r.log.Info("Configmap is not found, creating one")
		if err := r.Client.Create(ctx, cmDef); err != nil {
			r.log.Error(err, "Failed to create sshd configmap")
			return err
		}

	} else if err != nil {
		r.log.Error(err, "Failed to gather sshd configmap info")
		return err

	} else {
		r.log.Info("Updating existing ssh configmap")
		if err := r.Client.Update(ctx, cmDef); err != nil {
			r.log.Error(err, "Failed to update sshd configmap")
			return err
		}
	}

	cmRef, err := ref.GetReference(r.Scheme, cmDef)
	if err != nil {
		return err
	}

	r.resDef.Status.SSHdConfig = *cmRef
	return nil
}
