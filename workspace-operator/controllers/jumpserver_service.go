package controllers

import (
	"context"
	"encoding/json"
	"fmt"

	ref "k8s.io/client-go/tools/reference"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"

	corev1 "k8s.io/api/core/v1"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/apimachinery/pkg/util/intstr"
)

// ServicePortsEqual performs a deep equal between two arrays of service ports
// and ignores `NodePort`
func ServicePortsEqual(sp1, sp2 []corev1.ServicePort) bool {
	for i, port := range sp1 {
		if port.Name != sp2[i].Name ||
			port.Port != sp2[i].Port ||
			port.TargetPort != sp2[i].TargetPort {
			return false
		}
	}

	return true
}

func (r *JumpServerReconciler) buildService() *corev1.Service {
	labels := map[string]string{
		"app": "workspace-jumpserver",
	}

	svc := &corev1.Service{
		ObjectMeta: metav1.ObjectMeta{
			Name:      fmt.Sprintf("%s-jumpserver", r.resDef.Name),
			Namespace: r.resDef.Namespace,
			Labels:    labels,
		},
		Spec: corev1.ServiceSpec{
			Type:     *r.resDef.Spec.SSHServer.ServiceType,
			Selector: labels,
			Ports: []corev1.ServicePort{
				{
					Name:       "ssh",
					Port:       r.resDef.Spec.SSHServer.ServicePort,
					TargetPort: intstr.FromInt(22),
					Protocol:   corev1.ProtocolTCP,
				},
			},
		},
	}

	return svc
}

func (r *JumpServerReconciler) createService() error {
	ctx := context.Background()

	// build service
	svcDef := r.buildService()
	// add ownership
	if err := controllerutil.SetControllerReference(
		r.resDef,
		svcDef,
		r.Scheme,
	); err != nil {
		return err
	}

	// get existing service
	svc := &corev1.Service{}
	err := r.Client.Get(
		ctx,
		client.ObjectKey{
			Namespace: svcDef.GetObjectMeta().GetNamespace(),
			Name:      svcDef.GetObjectMeta().GetName(),
		},
		svc,
	)

	// create if not found
	if apierrors.IsNotFound(err) {
		r.log.Info("Service is not found, creating one")
		if err := r.Client.Create(ctx, svcDef); err != nil {
			r.log.Error(err, "Failed to create JumpServer Service")
			return err
		}

	} else if err != nil {
		r.log.Error(err, "Failed to gather service info")
		return err

	} else if svc.Spec.Type != svcDef.Spec.Type ||
		!ServicePortsEqual(svc.Spec.Ports, svcDef.Spec.Ports) {
		r.log.Info("Updating existing JumpServer service")

		patch, jsonErr := json.Marshal(svcDef)
		if jsonErr != nil {
			r.log.Error(jsonErr, "Failed to create service update patch")
			return jsonErr
		}

		if err := r.Client.Patch(
			ctx,
			svc,
			client.RawPatch(types.MergePatchType, patch),
		); err != nil {
			r.log.Error(err, "Failed to update JumpServer Service")
			return err
		}
	}

	svcRef, err := ref.GetReference(r.Scheme, svcDef)
	if err != nil {
		return err
	}

	r.resDef.Status.Service = *svcRef
	return nil
}
