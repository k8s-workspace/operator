package controllers

import (
	"context"

	"github.com/sethvargo/go-password/password"
	ref "k8s.io/client-go/tools/reference"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"

	corev1 "k8s.io/api/core/v1"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func (r *WorkspaceReconciler) buildPasswordSecret() *corev1.Secret {
	labels := map[string]string{
		"k8sWorkspaceSecretType": "user-password",
	}

	passwd, err := password.Generate(10, 3, 3, false, false)

	if err != nil {
		passwd = "defaultpassword"
	}

	s := &corev1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Name:      r.resDef.Spec.User.PasswordSecretName,
			Namespace: r.resDef.Namespace,
			Labels:    labels,
		},
		StringData: map[string]string{
			"password": passwd,
		},
	}

	return s
}

func (r *WorkspaceReconciler) createPasswordSecret() error {
	ctx := context.Background()

	// get existing secret
	s := &corev1.Secret{}
	err := r.Client.Get(
		ctx,
		client.ObjectKey{
			Name:      r.resDef.Spec.User.PasswordSecretName,
			Namespace: r.resDef.Namespace,
		},
		s,
	)

	secretRef := &corev1.ObjectReference{}

	// create if not found
	if apierrors.IsNotFound(err) {
		// build Secret
		secretDef := r.buildPasswordSecret()

		// add ownership
		if err := controllerutil.SetControllerReference(
			r.resDef,
			secretDef,
			r.Scheme,
		); err != nil {
			return err
		}

		r.log.Info("password secret is not found, creating one")
		if err := r.Client.Create(ctx, secretDef); err != nil {
			r.log.Error(err, "Failed to create workspace password secret")
			return err
		}

		sr, err := ref.GetReference(r.Scheme, secretDef)
		if err != nil {
			return err
		}

		secretRef = sr

	} else if err != nil {
		r.log.Error(err, "Failed to gather password secret info")
		return err

	} else {
		sr, err := ref.GetReference(r.Scheme, s)
		if err != nil {
			return err
		}

		secretRef = sr
	}

	r.resDef.Status.PasswordSecret = *secretRef

	return nil
}
