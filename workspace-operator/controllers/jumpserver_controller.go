/*


Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"fmt"

	"github.com/go-logr/logr"
	"k8s.io/apimachinery/pkg/runtime"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/event"
	"sigs.k8s.io/controller-runtime/pkg/predicate"

	workspacev1 "gitlab.com/bailinhe/k8s-workspace/api/v1"
)

const (
	ownerKey = ".metadata.controller"
)

// JumpServerReconciler reconciles a JumpServer object
type JumpServerReconciler struct {
	client.Client
	Scheme *runtime.Scheme
	log    logr.Logger
	Log    logr.Logger
	resDef *workspacev1.JumpServer
}

// +kubebuilder:rbac:groups=workspace.bailinhe.com,resources=jumpservers,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=workspace.bailinhe.com,resources=jumpservers/status,verbs=get;update;patch
// +kubebuilder:rbac:groups=apps,resources=deployments,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=core,resources=secrets,verbs=get;list;watch;create;update;patch;delete;
// +kubebuilder:rbac:groups=core,resources=services,verbs=get;list;watch;create;update;patch;delete;
// +kubebuilder:rbac:groups=core,resources=persistentvolumeclaims,verbs=get;list;watch;create;update;patch;delete;
// +kubebuilder:rbac:groups=core,resources=configmaps,verbs=get;list;watch;create;update;patch;delete;
// +kubebuilder:rbac:groups=core,resources=pods,verbs=get;list;

// Reconcile implements the reconcile loop. The reconcile loop is passed the
// Request argument which is a Namespace/Name key used to lookup the primary
// resource object from the cache
func (r *JumpServerReconciler) Reconcile(req ctrl.Request) (ctrl.Result, error) {
	ctx := context.Background()
	r.log = r.Log.WithValues("jumpserver", req.NamespacedName)

	// get JumpServer specs
	r.resDef = &workspacev1.JumpServer{}
	if err := r.Client.Get(ctx, req.NamespacedName, r.resDef); err != nil {
		return ctrl.Result{}, client.IgnoreNotFound(err)
	}

	// create auth-key secret
	if err := r.createAuthKeys(); err != nil {
		r.updateResourceStatusOnError(err)
		return ctrl.Result{}, err
	}

	// create PVC
	if err := r.createPVC(); err != nil {
		r.updateResourceStatusOnError(err)
		return ctrl.Result{}, err
	}

	// create SSHd configmap
	if err := r.createSSHdConfigMap(); err != nil {
		r.updateResourceStatusOnError(err)
		return ctrl.Result{}, err
	}

	// create deployment
	if err := r.deployment(); err != nil {
		r.updateResourceStatusOnError(err)
		return ctrl.Result{}, err
	}

	// create service
	if err := r.createService(); err != nil {
		r.updateResourceStatusOnError(err)
		return ctrl.Result{}, err
	}

	r.resDef.Status.ReconcileSuccess = true
	r.resDef.Status.Message = "Success"

	if err := r.Client.Status().Update(ctx, r.resDef); err != nil {
		return ctrl.Result{}, err
	}

	return ctrl.Result{}, nil
}

func ensureGenerationIsChanged() predicate.Predicate {
	return predicate.Funcs{
		UpdateFunc: func(e event.UpdateEvent) bool {
			return e.MetaNew.GetGeneration() != e.MetaOld.GetGeneration()
		},
	}
}

// SetupWithManager specifies how the controller is built to watch a CR and
// other resources that are owned and managed by that controller
func (r *JumpServerReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&workspacev1.JumpServer{}).
		WithEventFilter(ensureGenerationIsChanged()).
		Complete(r)
}

func (r *JumpServerReconciler) updateResourceStatusOnError(err error) {
	r.resDef.Status.ReconcileSuccess = false
	r.resDef.Status.Message = fmt.Sprintf("Error:\t%s", err.Error())
	_ = r.Client.Status().Update(context.Background(), r.resDef)
}
