package controllers

import (
	"context"
	"fmt"

	corev1 "k8s.io/api/core/v1"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"

	authorizedkeys "gitlab.com/bailinhe/k8s-workspace/controllers/authorized-keys"
)

func (r *PublicKeyReconciler) markDeletingSecret(ns string, name string) error {
	ctx := context.Background()
	secret := &corev1.Secret{}

	if err := r.Get(
		ctx,
		client.ObjectKey{
			Namespace: ns,
			Name:      fmt.Sprintf("%s-publickey", name),
		},
		secret,
	); err != nil {
		return client.IgnoreNotFound(err)
	}

	labels := secret.Labels
	labels["deleting"] = "true"

	secret.Labels = labels
	if err := r.Update(
		ctx,
		secret,
	); err != nil {
		return client.IgnoreNotFound(err)
	}

	return nil
}

func (r *PublicKeyReconciler) ensureAuthKeys(
	keyType string,
	namespace string,
) (*corev1.SecretList, error) {

	ctx := context.Background()
	secrets := &corev1.SecretList{}

	if keyType == "jumpserver" {
		if err := r.List(
			ctx,
			secrets,
			client.MatchingLabels{
				"k8sWorkspaceSecretType": fmt.Sprintf("%s-authorized-keys", keyType),
			},
		); err != nil {
			return nil, err
		}
	} else {
		if err := r.List(
			ctx,
			secrets,
			client.InNamespace(namespace),
			client.MatchingLabels{
				"k8sWorkspaceSecretType": fmt.Sprintf("%s-authorized-keys", keyType),
			},
		); err != nil {
			return nil, err
		}

	}

	return secrets, nil
}

func (r *PublicKeyReconciler) updateAuthKeysWithType(
	onDelete bool,
	req ctrl.Request,
	authKeyType string,
) error {
	r.log.Info(fmt.Sprintf("Updating %s auth-keys", authKeyType))

	ctx := context.Background()
	authorizedKeysSecrets, err := r.ensureAuthKeys(authKeyType, req.Namespace)

	if err != nil {
		return err
	}

	if len(authorizedKeysSecrets.Items) == 0 && !onDelete {
		msg := fmt.Sprintf(
			"\n\t - Did not update %s auth-key secrets, "+
				"possibly because %s is not deployed",
			authKeyType,
			authKeyType,
		)
		r.resDef.Status.Message = fmt.Sprintf("%s%s", msg, r.resDef.Status.Message)
		return nil
	}

	publicKeySecrets := &corev1.SecretList{}

	if authKeyType == "jumpserver" {
		if err := r.List(
			ctx,
			publicKeySecrets,
			client.MatchingLabels{
				"k8sWorkspaceSecretType": "publickey",
			},
		); err != nil {
			return err
		}
	} else {
		if err := r.List(
			ctx,
			publicKeySecrets,
			client.InNamespace(req.Namespace),
			client.MatchingLabels{
				"k8sWorkspaceSecretType": "publickey",
			},
		); err != nil {
			return err
		}

	}

	for _, authKeySecret := range authorizedKeysSecrets.Items {
		ak := &authorizedkeys.AuthorizedKeys{
			PublicKeySecrets: publicKeySecrets,
		}

		if authKeyType == "jumpserver" {
			ak.BuildAuthKeys("default")
		} else {
			ak.BuildAuthKeys("")
		}

		authKeySecret.Data = map[string][]byte{
			"authorized_keys": []byte(ak.DataStr),
		}

		if err := r.Client.Update(
			context.Background(),
			&authKeySecret,
		); err != nil {
			return err
		}
	}

	return nil
}

func (r *PublicKeyReconciler) updateAuthKeys(
	onDelete bool,
	req ctrl.Request,
) error {

	if onDelete {
		r.log.Info(fmt.Sprintf("Deleting %s-publickey @ %s", req.Name, req.Namespace))
		r.markDeletingSecret(req.Namespace, req.Name)
	}

	r.updateAuthKeysWithType(onDelete, req, "jumpserver")
	r.updateAuthKeysWithType(onDelete, req, "workspace")

	return nil
}
