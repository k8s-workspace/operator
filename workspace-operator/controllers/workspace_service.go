package controllers

import (
	"context"
	"encoding/json"
	"fmt"

	ref "k8s.io/client-go/tools/reference"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"

	corev1 "k8s.io/api/core/v1"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/apimachinery/pkg/util/intstr"
)

func (r *WorkspaceReconciler) buildService() *corev1.Service {
	labels := map[string]string{
		"app": "workspace",
	}

	svc := &corev1.Service{
		ObjectMeta: metav1.ObjectMeta{
			Name:      fmt.Sprintf("%s-workspace", r.resDef.Name),
			Namespace: r.resDef.Namespace,
			Labels:    labels,
		},
		Spec: corev1.ServiceSpec{
			Type:     r.resDef.Spec.ServiceType,
			Selector: labels,
			Ports: append(
				r.resDef.Spec.ServicePorts,
				corev1.ServicePort{
					Name:       "ssh",
					Port:       22,
					TargetPort: intstr.FromInt(22),
					Protocol:   corev1.ProtocolTCP,
				},
			),
		},
	}

	return svc
}

func (r *WorkspaceReconciler) createService() error {
	ctx := context.Background()

	// build service
	svcDef := r.buildService()
	// add ownership
	if err := controllerutil.SetControllerReference(
		r.resDef,
		svcDef,
		r.Scheme,
	); err != nil {
		return err
	}

	// get existing service
	svc := &corev1.Service{}
	err := r.Client.Get(
		ctx,
		client.ObjectKey{
			Namespace: svcDef.GetObjectMeta().GetNamespace(),
			Name:      svcDef.GetObjectMeta().GetName(),
		},
		svc,
	)

	// create if not found
	if apierrors.IsNotFound(err) {
		r.log.Info("Service is not found, creating one")
		if err := r.Client.Create(ctx, svcDef); err != nil {
			r.log.Error(err, "Failed to create Workspace Service")
			return err
		}

	} else if err != nil {
		r.log.Error(err, "Failed to gather service info")
		return err

	} else {
		r.log.Info("Updating existing Workspace service")

		patch, jsonErr := json.Marshal(svcDef)
		if jsonErr != nil {
			r.log.Error(jsonErr, "Failed to create service update patch")
			return jsonErr
		}

		if err := r.Client.Patch(
			ctx,
			svc,
			client.RawPatch(types.MergePatchType, patch),
		); err != nil {
			r.log.Error(err, "Failed to update Workspace Service")
			return err
		}
	}

	svcRef, err := ref.GetReference(r.Scheme, svcDef)
	if err != nil {
		return err
	}

	r.resDef.Status.Service = *svcRef
	return nil
}
