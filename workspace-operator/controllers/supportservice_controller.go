/*


Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"fmt"

	"github.com/go-logr/logr"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"

	workspacev1 "gitlab.com/bailinhe/k8s-workspace/api/v1"
)

func containsString(slice []string, s string) bool {
	for _, item := range slice {
		if item == s {
			return true
		}
	}
	return false
}

func removeString(slice []string, s string) (result []string) {
	for _, item := range slice {
		if item == s {
			continue
		}
		result = append(result, item)
	}
	return result
}

// SupportServiceReconciler reconciles a SupportService object
type SupportServiceReconciler struct {
	client.Client
	Log    logr.Logger
	Scheme *runtime.Scheme

	log           logr.Logger
	resDef        *workspacev1.SupportService
	workspace     *workspacev1.Workspace
	finalizerName string
}

// +kubebuilder:rbac:groups=workspace.bailinhe.com,resources=supportservices,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=workspace.bailinhe.com,resources=supportservices/status,verbs=get;update;patch

// Reconcile implements the reconcile loop. The reconcile loop is passed the
// Request argument which is a Namespace/Name key used to lookup the primary
// resource object from the cache
func (r *SupportServiceReconciler) Reconcile(req ctrl.Request) (ctrl.Result, error) {
	ctx := context.Background()
	r.log = r.Log.WithValues("supportservice", req.NamespacedName)

	r.finalizerName = "supportservice.finalizers.workspace.bailinhe.com"

	// get SupportService specs
	r.resDef = &workspacev1.SupportService{}
	if err := r.Client.Get(ctx, req.NamespacedName, r.resDef); err != nil {
		return ctrl.Result{}, client.IgnoreNotFound(err)
	}

	onDelete := !r.resDef.ObjectMeta.DeletionTimestamp.IsZero()

	// ensure workspace exists
	wsExists, wsFoundErr := r.ensureWorkspace()
	if wsFoundErr != nil {
		r.updateResourceStatusOnError(wsFoundErr)
		return ctrl.Result{}, wsFoundErr
	}

	if !wsExists {
		r.updateResourceStatusOnError(
			fmt.Errorf("Workspace `%s` is not found", r.resDef.Spec.For),
		)
		return ctrl.Result{}, nil
	}

	if !onDelete {
		// add ownership
		if err := controllerutil.SetControllerReference(
			r.workspace,
			r.resDef,
			r.Scheme,
		); err != nil {
			return ctrl.Result{}, err
		}

		// add finalizer
		if !containsString(r.resDef.ObjectMeta.Finalizers, r.finalizerName) {
			r.resDef.ObjectMeta.Finalizers = append(
				r.resDef.ObjectMeta.Finalizers,
				r.finalizerName,
			)
		}

		if err := r.Client.Update(ctx, r.resDef); err != nil {
			r.updateResourceStatusOnError(err)
			return ctrl.Result{}, err
		}
	}

	if err := r.createPVC(); err != nil {
		r.updateResourceStatusOnError(err)
		return ctrl.Result{}, err
	}

	if err := r.updateWorkspaceDeployment(onDelete); err != nil {
		r.updateResourceStatusOnError(err)
		return ctrl.Result{}, err
	}

	if err := r.updateWorkspaceService(onDelete); err != nil {
		r.updateResourceStatusOnError(err)
		return ctrl.Result{}, err
	}

	if !onDelete {
		r.resDef.Status.ReconcileSuccess = true
		r.resDef.Status.Message = "Success"
		if err := r.Client.Status().Update(ctx, r.resDef); err != nil {
			return ctrl.Result{}, err
		}

	} else {
		r.resDef.ObjectMeta.Finalizers = removeString(
			r.resDef.ObjectMeta.Finalizers,
			r.finalizerName,
		)

		r.Client.Update(ctx, r.resDef)
	}

	return ctrl.Result{}, nil
}

// SetupWithManager specifies how the controller is built to watch a CR and
// other resources that are owned and managed by that controller
func (r *SupportServiceReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&workspacev1.SupportService{}).
		WithEventFilter(ensureGenerationIsChanged()).
		Complete(r)
}

func (r *SupportServiceReconciler) updateResourceStatusOnError(err error) {
	r.resDef.Status.ReconcileSuccess = false
	r.resDef.Status.Message = fmt.Sprintf("Error:\t%s", err.Error())
	_ = r.Client.Status().Update(context.Background(), r.resDef)
}

func (r *SupportServiceReconciler) ensureWorkspace() (bool, error) {
	ctx := context.Background()

	r.workspace = &workspacev1.Workspace{}
	err := r.Client.Get(
		ctx,
		client.ObjectKey{
			Name:      r.resDef.Spec.For,
			Namespace: r.resDef.Namespace,
		},
		r.workspace,
	)

	if apierrors.IsNotFound(err) {
		return false, nil

	} else if err != nil {
		return false, err
	}

	return true, nil
}
