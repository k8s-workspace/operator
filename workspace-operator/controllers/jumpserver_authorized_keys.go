package controllers

import (
	"context"

	authorizedkeys "gitlab.com/bailinhe/k8s-workspace/controllers/authorized-keys"

	ref "k8s.io/client-go/tools/reference"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"

	corev1 "k8s.io/api/core/v1"
)

func (r *JumpServerReconciler) createAuthKeys() error {
	ctx := context.Background()

	// grab the public keys
	publicKeySecrets := &corev1.SecretList{}
	if err := r.Client.List(
		context.Background(),
		publicKeySecrets,
		client.MatchingLabels{
			"k8sWorkspaceSecretType": "publickey",
		},
	); err != nil {
		return err
	}

	// build secret
	ak := &authorizedkeys.AuthorizedKeys{}
	if err := ak.BuildAuthorizedKeysSecretDefinition(
		"jumpserver-authorized-keys",
		r.resDef.Namespace,
		"jumpserver-authorized-keys",
		publicKeySecrets,
		"default",
	); err != nil {
		return err
	}

	secretDef := ak.AuthorizedKeysSecret

	// add ownership
	if err := controllerutil.SetControllerReference(
		r.resDef,
		secretDef,
		r.Scheme,
	); err != nil {
		return err
	}

	// create secret
	if err := ak.CreateSecret(ctx, secretDef, r.Client, r.log); err != nil {
		return err
	}

	secretRef, err := ref.GetReference(r.Scheme, secretDef)
	if err != nil {
		return err
	}

	r.resDef.Status.AuthorizedKeysSecret = *secretRef
	return nil
}
