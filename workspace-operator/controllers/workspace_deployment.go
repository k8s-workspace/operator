package controllers

import (
	"context"
	"fmt"

	ref "k8s.io/client-go/tools/reference"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"

	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"

	apierrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	workspacedefaults "gitlab.com/bailinhe/k8s-workspace/controllers/workspace-defaults"
)

func (r *WorkspaceReconciler) buildDeployment() *appsv1.Deployment {
	labels := map[string]string{
		"app": "workspace",
	}

	volumes := []corev1.Volume{
		{
			Name: "authorized-keys",
			VolumeSource: corev1.VolumeSource{
				Secret: &corev1.SecretVolumeSource{
					SecretName: "workspace-authorized-keys",
				},
			},
		},
		{
			Name: "home",
			VolumeSource: corev1.VolumeSource{
				PersistentVolumeClaim: &corev1.PersistentVolumeClaimVolumeSource{
					ClaimName: fmt.Sprintf("%s-workspace-home", r.resDef.Name),
				},
			},
		},
		{
			Name: "password",
			VolumeSource: corev1.VolumeSource{
				Secret: &corev1.SecretVolumeSource{
					SecretName: r.resDef.Spec.User.PasswordSecretName,
				},
			},
		},
		{
			Name: "sshd-config",
			VolumeSource: corev1.VolumeSource{
				ConfigMap: &corev1.ConfigMapVolumeSource{
					LocalObjectReference: corev1.LocalObjectReference{
						Name: fmt.Sprintf("%s-workspace-sshd-config", r.resDef.Name),
					},
				},
			},
		},
		{
			Name: "ssh-workspace-config",
			VolumeSource: corev1.VolumeSource{
				PersistentVolumeClaim: &corev1.PersistentVolumeClaimVolumeSource{
					ClaimName: fmt.Sprintf("%s-ssh-workspace-config", r.resDef.Name),
				},
			},
		},
	}

	containers := []corev1.Container{
		*r.resDef.Spec.Container,
		{
			Image:           workspacedefaults.ConfigReloadImage,
			ImagePullPolicy: "IfNotPresent",
			Name:            "authorized-keys-reload",
			Resources:       r.resDef.Spec.AuthorizedKeysReload.Resources,
			Env: []corev1.EnvVar{
				{
					Name:  "PUID",
					Value: fmt.Sprintf("%d", r.resDef.Spec.User.UID),
				},
				{
					Name:  "PGID",
					Value: fmt.Sprintf("%d", r.resDef.Spec.User.GID),
				},
				{
					Name:  "DOT_SSH",
					Value: fmt.Sprintf("/workspace-home/%s/.ssh", r.resDef.Spec.User.Username),
				},
			},
			VolumeMounts: []corev1.VolumeMount{
				{
					Name:      "home",
					MountPath: "/workspace-home",
				},
				{
					Name:      "authorized-keys",
					MountPath: "/auth-keys-secrets",
				},
				{
					Name:      "ssh-workspace-config",
					MountPath: "/ssh",
				},
				{
					Name:      "sshd-config",
					MountPath: "/sshd-config",
				},
			},
			SecurityContext: &corev1.SecurityContext{
				Capabilities: &corev1.Capabilities{
					Add: []corev1.Capability{
						"SYS_PTRACE",
					},
				},
			},
		},
	}

	containers[0].VolumeMounts = append(
		containers[0].VolumeMounts,
		corev1.VolumeMount{
			Name:      "home",
			MountPath: "/home",
		},
		corev1.VolumeMount{
			Name:      "password",
			MountPath: "/secret",
		},
		corev1.VolumeMount{
			Name:      "ssh-workspace-config",
			MountPath: "/etc/ssh",
		},
	)

	containers[0].Env = append(
		containers[0].Env,
		corev1.EnvVar{
			Name:  "PUID",
			Value: fmt.Sprintf("%d", r.resDef.Spec.User.UID),
		},
		corev1.EnvVar{
			Name:  "PGID",
			Value: fmt.Sprintf("%d", r.resDef.Spec.User.GID),
		},
		corev1.EnvVar{
			Name:  "USER_NAME",
			Value: r.resDef.Spec.User.Username,
		},
	)

	containers[0].Ports = append(
		containers[0].Ports,
		corev1.ContainerPort{
			ContainerPort: 22,
		},
	)

	containers[0].SecurityContext = &corev1.SecurityContext{
		Capabilities: &corev1.Capabilities{
			Add: []corev1.Capability{
				"SYS_PTRACE",
			},
		},
	}

	shareProcessNamespace := true

	deployment := &appsv1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name:      fmt.Sprintf("%s-workspace", r.resDef.Name),
			Namespace: r.resDef.Namespace,
			Labels:    labels,
		},

		Spec: appsv1.DeploymentSpec{
			Selector: &metav1.LabelSelector{
				MatchLabels: labels,
			},

			Strategy: appsv1.DeploymentStrategy{
				Type: "Recreate",
			},

			Template: corev1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Labels: labels,
				},

				Spec: corev1.PodSpec{
					Volumes:               volumes,
					Containers:            containers,
					ShareProcessNamespace: &shareProcessNamespace,
				},
			},
		},
	}

	return deployment
}

func (r *WorkspaceReconciler) deployment() error {
	ctx := context.Background()

	// build deployment
	deploymentDef := r.buildDeployment()
	// add ownership
	if err := controllerutil.SetControllerReference(
		r.resDef,
		deploymentDef,
		r.Scheme,
	); err != nil {
		return err
	}

	// get existing deployment
	deployment := appsv1.Deployment{}
	err := r.Client.Get(
		ctx,
		client.ObjectKey{
			Namespace: deploymentDef.GetObjectMeta().GetNamespace(),
			Name:      deploymentDef.GetObjectMeta().GetName(),
		},
		&deployment,
	)

	// deploy if not found
	if apierrors.IsNotFound(err) {
		r.log.Info("Deployment is not found, creating one")

		if err := r.Client.Create(ctx, deploymentDef); err != nil {
			r.log.Error(err, "Failed to create deployment")

			return err
		}

	} else if err != nil {
		r.log.Error(err, "Failed to gather deployment info")
		return err

	} else {
		// update if found
		r.log.Info("Updating existing deployment")

		if err := r.Client.Update(ctx, deploymentDef); err != nil {
			r.log.Error(err, "Failed to update deployment")
			return err
		}
	}

	deploymentRef, err := ref.GetReference(r.Scheme, deploymentDef)

	if err != nil {
		return err
	}

	r.resDef.Status.Deployment = *deploymentRef

	return nil
}
