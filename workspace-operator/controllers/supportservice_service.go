package controllers

import (
	"context"
	"encoding/json"

	"sigs.k8s.io/controller-runtime/pkg/client"

	corev1 "k8s.io/api/core/v1"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/types"
)

func (r *SupportServiceReconciler) buildServicePatch(
	svc *corev1.Service,
	onDelete bool,
) {

	// create map of existing service ports:
	// portNumber -> { servicePort, index }
	type existingServicePortsMapData struct {
		Index       int
		ServicePort *corev1.ServicePort
	}

	existingServicePortsMap := map[int32]*existingServicePortsMapData{}
	for i, sp := range svc.Spec.Ports {
		existingServicePortsMap[sp.Port] = &existingServicePortsMapData{
			Index:       i,
			ServicePort: &sp,
		}
	}

	// for each service port in resource definition, find if it already exists
	// in the service's definition, replace if it does, append if it does not
	// remove on delete
	for _, sp := range r.resDef.Spec.ServicePorts {
		if !onDelete {
			// on create / update
			if existingServicePortsMap[sp.Port] != nil {
				// found
				*existingServicePortsMap[sp.Port].ServicePort = sp
			} else {
				// not found
				svc.Spec.Ports = append(svc.Spec.Ports, sp)
			}
		} else {
			// on delete
			if existingServicePortsMap[sp.Port] != nil {
				// found
				i := existingServicePortsMap[sp.Port].Index
				svc.Spec.Ports = append(svc.Spec.Ports[:i], svc.Spec.Ports[i+1:]...)
			}
		}
	}
}

func (r *SupportServiceReconciler) updateWorkspaceService(
	onDelete bool,
) error {
	ctx := context.Background()

	// get existing service
	svc := &corev1.Service{}
	err := r.Client.Get(
		ctx,
		client.ObjectKey{
			Namespace: r.resDef.Namespace,
			Name:      r.workspace.Status.Service.Name,
		},
		svc,
	)

	if apierrors.IsNotFound(err) {
		r.log.Error(err, "Workspace service is not found")
		return err
	} else if err != nil {
		r.log.Error(err, "Failed to gather workspace service info")
		return err
	}

	// update if found
	r.log.Info("Updating existing service")

	r.buildServicePatch(svc, onDelete)

	patch, jsonErr := json.Marshal(svc)
	if jsonErr != nil {
		r.log.Error(jsonErr, "Failed to create service update patch")
		return jsonErr
	}

	if err := r.Client.Patch(
		ctx,
		svc,
		client.RawPatch(types.MergePatchType, patch),
	); err != nil {
		r.log.Error(err, "Failed to update workspace service")
		return err
	}

	return nil
}
