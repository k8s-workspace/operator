package authorizedkeys

import (
	"context"
	"fmt"
	"strings"

	"github.com/go-logr/logr"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	"sigs.k8s.io/controller-runtime/pkg/client"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// AuthorizedKeys builds authorized_keys file from public key secrets
type AuthorizedKeys struct {
	PublicKeySecrets     *corev1.SecretList
	AuthorizedKeysSecret *corev1.Secret
	DataStr              string
}

// BuildAuthKeys builds authorizes_keys file string and save it in DataStr
func (ak *AuthorizedKeys) BuildAuthKeys(options string) error {
	if options == "default" {
		options = "command=\"echo Hello!\",restrict,port-forwarding"
	}

	for _, secret := range ak.PublicKeySecrets.Items {
		if secret.Labels["deleting"] == "true" {
			continue
		}

		entry := fmt.Sprintf(
			"%s %s\n",
			options,
			strings.TrimSuffix(string(secret.Data["id_rsa_pub"]), "\n"),
		)

		ak.DataStr += entry
	}

	return nil
}

// BuildAuthorizedKeysSecretDefinition builds a secret based on the auth-key
// generated above
func (ak *AuthorizedKeys) BuildAuthorizedKeysSecretDefinition(
	name string,
	namespace string,
	k8sWorkspaceSecretType string,
	publicKeySecrets *corev1.SecretList,
	options string,
) error {

	ak.PublicKeySecrets = publicKeySecrets

	labels := map[string]string{
		"k8sWorkspaceSecretType": k8sWorkspaceSecretType,
	}

	ak.BuildAuthKeys(options)

	data := map[string][]byte{
		"authorized_keys": []byte(ak.DataStr),
	}

	secret := &corev1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: namespace,
			Labels:    labels,
		},

		Data: data,
	}

	ak.AuthorizedKeysSecret = secret

	return nil
}

// CreateSecret creates a secret with a given definition
func (ak *AuthorizedKeys) CreateSecret(
	ctx context.Context,
	secretDef *corev1.Secret,
	reconcilerClient client.Client,
	log logr.Logger,
) error {

	// get existing secret
	secret := &corev1.Secret{}
	err := reconcilerClient.Get(
		context.Background(),
		client.ObjectKey{
			Namespace: secretDef.GetObjectMeta().GetNamespace(),
			Name:      secretDef.GetObjectMeta().GetName(),
		},
		secret,
	)

	// deploy if not found
	if apierrors.IsNotFound(err) {
		log.Info("Authorized-keys secret is not found, creating one")

		if err := reconcilerClient.Create(ctx, secretDef); err != nil {
			log.Error(err, "Failed to create auth-keys secret")
			return err
		}

	} else if err != nil {
		log.Error(err, "Failed to gather auth-keys secret info")
		return err
	}

	return nil
}
