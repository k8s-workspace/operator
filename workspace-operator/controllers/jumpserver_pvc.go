package controllers

import (
	"context"

	workspacev1 "gitlab.com/bailinhe/k8s-workspace/api/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"

	corev1 "k8s.io/api/core/v1"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func (r *JumpServerReconciler) buildPVC(resDef *workspacev1.JumpServer) *corev1.PersistentVolumeClaim {
	labels := map[string]string{
		"app": "workspace-jumpserver",
	}

	pvc := &corev1.PersistentVolumeClaim{
		ObjectMeta: metav1.ObjectMeta{
			Name:      "ssh-jump-server-config",
			Namespace: resDef.Namespace,
			Labels:    labels,
		},
		Spec: corev1.PersistentVolumeClaimSpec{
			AccessModes: []corev1.PersistentVolumeAccessMode{
				"ReadWriteOnce",
			},
			Resources: corev1.ResourceRequirements{
				Requests: corev1.ResourceList{
					corev1.ResourceName(corev1.ResourceStorage): resource.MustParse("1Gi"),
				},
			},
		},
	}

	return pvc
}

func (r *JumpServerReconciler) createPVC() error {
	ctx := context.Background()

	// build PVC
	pvcDef := r.buildPVC(r.resDef)
	// add ownership
	if err := controllerutil.SetControllerReference(
		r.resDef,
		pvcDef,
		r.Scheme,
	); err != nil {
		return err
	}

	// get existing PVC
	pvc := &corev1.PersistentVolumeClaim{}
	err := r.Client.Get(
		ctx,
		client.ObjectKey{
			Namespace: pvcDef.GetObjectMeta().GetNamespace(),
			Name:      pvcDef.GetObjectMeta().GetName(),
		},
		pvc,
	)

	// create if not found
	if apierrors.IsNotFound(err) {
		r.log.Info("PVC is not found, creating one")
		if err := r.Client.Create(ctx, pvcDef); err != nil {
			r.log.Error(err, "Failed to create auth-keys PVC")
			return err
		}

	} else if err != nil {
		r.log.Error(err, "Failed to gather PVC info")
		return err
	}

	return nil
}
