package controllers

import (
	"context"
	"fmt"

	"k8s.io/apimachinery/pkg/api/resource"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"

	corev1 "k8s.io/api/core/v1"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func (r *WorkspaceReconciler) homePVC() *corev1.PersistentVolumeClaim {
	labels := map[string]string{
		"app": "workspace",
	}

	pvc := &corev1.PersistentVolumeClaim{
		ObjectMeta: metav1.ObjectMeta{
			Name:      fmt.Sprintf("%s-workspace-home", r.resDef.Name),
			Namespace: r.resDef.Namespace,
			Labels:    labels,
		},
		Spec: *r.resDef.Spec.Home.PVC,
	}

	return pvc
}

func (r *WorkspaceReconciler) sshdConfigPVC() *corev1.PersistentVolumeClaim {
	labels := map[string]string{
		"app": "workspace",
	}

	pvc := &corev1.PersistentVolumeClaim{
		ObjectMeta: metav1.ObjectMeta{
			Name:      fmt.Sprintf("%s-ssh-workspace-config", r.resDef.Name),
			Namespace: r.resDef.Namespace,
			Labels:    labels,
		},
		Spec: corev1.PersistentVolumeClaimSpec{
			AccessModes: []corev1.PersistentVolumeAccessMode{
				"ReadWriteOnce",
			},
			Resources: corev1.ResourceRequirements{
				Requests: corev1.ResourceList{
					corev1.ResourceName(corev1.ResourceStorage): resource.MustParse("1Gi"),
				},
			},
		},
	}

	return pvc
}

func (r *WorkspaceReconciler) createPVC() error {
	ctx := context.Background()

	// build PVC
	homePVCDef := r.homePVC()
	// add ownership
	if err := controllerutil.SetControllerReference(
		r.resDef,
		homePVCDef,
		r.Scheme,
	); err != nil {
		return err
	}

	// get existing PVC
	pvc := &corev1.PersistentVolumeClaim{}
	err := r.Client.Get(
		ctx,
		client.ObjectKey{
			Namespace: homePVCDef.Namespace,
			Name:      homePVCDef.Name,
		},
		pvc,
	)

	// create if not found
	if apierrors.IsNotFound(err) {
		r.log.Info("PVC is not found, creating one")
		if err := r.Client.Create(ctx, homePVCDef); err != nil {
			r.log.Error(err, "Failed to create workspace home PVC")
			return err
		}

	} else if err != nil {
		r.log.Error(err, "Failed to gather PVC info")
		return err
	}

	// build PVC
	sshdConfigPVCDef := r.sshdConfigPVC()
	// add ownership
	if err := controllerutil.SetControllerReference(
		r.resDef,
		sshdConfigPVCDef,
		r.Scheme,
	); err != nil {
		return err
	}

	// get existing PVC
	sshdPVC := &corev1.PersistentVolumeClaim{}
	sshdPVCErr := r.Client.Get(
		ctx,
		client.ObjectKey{
			Namespace: sshdConfigPVCDef.Namespace,
			Name:      sshdConfigPVCDef.Name,
		},
		sshdPVC,
	)

	// create if not found
	if apierrors.IsNotFound(sshdPVCErr) {
		r.log.Info("SSHd config PVC is not found, creating one")
		if err := r.Client.Create(ctx, sshdConfigPVCDef); err != nil {
			r.log.Error(err, "Failed to create workspace sshd config PVC")
			return err
		}

	} else if err != nil {
		r.log.Error(err, "Failed to gather PVC info")
		return err
	}

	return nil
}
