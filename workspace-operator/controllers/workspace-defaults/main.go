package workspacedefaults

// SSHdConfig is the defualt config for sshd, used when SSHdConfig is not
// provided by the user
const SSHdConfig string = `
Port 22

HostKey /etc/ssh/ssh_host_rsa_key
HostKey /etc/ssh/ssh_host_dsa_key
HostKey /etc/ssh/ssh_host_ecdsa_key
HostKey /etc/ssh/ssh_host_ed25519_key

SyslogFacility AUTH
LogLevel INFO

LoginGraceTime 2m
PermitRootLogin no
StrictModes yes
MaxAuthTries 6
MaxSessions 10

PubkeyAuthentication yes
PasswordAuthentication no
PermitEmptyPasswords no
AuthorizedKeysFile      .ssh/authorized_keys
ChallengeResponseAuthentication no
UsePAM yes
X11Forwarding no
PrintMotd no
`

// ConfigReloadImage is the container image for reloading configs,
// authorizedKeys, etc..
const ConfigReloadImage = "registry.gitlab.com/k8s-workspace/images/config-reload:latest"

// JumpServerImg is the container image of the ssh jumphost
const JumpServerImg = "registry.gitlab.com/k8s-workspace/images/jumpserver:latest"

type helperCmdPreventingCrashLoopType struct {
	Command []string
	Args    []string
}

// HelperCmdPreventingCrashLoop is a simple command preventing a container
// from exiting
var HelperCmdPreventingCrashLoop = helperCmdPreventingCrashLoopType{
	Command: []string{
		"/bin/sh",
	},
	Args: []string{
		"-c",
		`
			set -x
			while true; do
				sleep 100
			done
		`,
	},
}
