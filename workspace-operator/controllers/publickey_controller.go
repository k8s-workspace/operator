/*


Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"fmt"

	"github.com/go-logr/logr"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"

	workspacev1 "gitlab.com/bailinhe/k8s-workspace/api/v1"
)

// PublicKeyReconciler reconciles a PublicKey object
type PublicKeyReconciler struct {
	client.Client
	Log    logr.Logger
	log    logr.Logger
	Scheme *runtime.Scheme
	resDef *workspacev1.PublicKey
}

// +kubebuilder:rbac:groups=workspace.bailinhe.com,resources=publickeys,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=workspace.bailinhe.com,resources=publickeys/status,verbs=get;update;patch
// +kubebuilder:rbac:groups=core,resources=secrets,verbs=get;list;watch;create;update;patch;delete;

// Reconcile implements the reconcile loop. The reconcile loop is passed the
// Request argument which is a Namespace/Name key used to lookup the primary
// resource object from the cache
func (r *PublicKeyReconciler) Reconcile(req ctrl.Request) (ctrl.Result, error) {
	ctx := context.Background()
	r.log = r.Log.WithValues("publickeys", req.NamespacedName)

	// get PublicKey specs
	r.resDef = &workspacev1.PublicKey{}
	if err := r.Client.Get(ctx, req.NamespacedName, r.resDef); err != nil {

		if errors.IsNotFound(err) {
			if e := r.updateAuthKeys(true, req); e != nil {
				err = e
			}
		}

		return ctrl.Result{}, client.IgnoreNotFound(err)
	}

	r.resDef.Status.Message = "\n"

	// create secret
	if err := r.createPublicKey(); err != nil {
		r.updateResourceStatusOnError(err)
		return ctrl.Result{}, err
	}

	if err := r.updateAuthKeys(false, req); err != nil {
		r.updateResourceStatusOnError(err)
		return ctrl.Result{}, err
	}

	r.resDef.Status.ReconcileSuccess = true
	r.resDef.Status.Message = fmt.Sprintf(
		"\n\t - Success%s",
		r.resDef.Status.Message,
	)

	if err := r.Client.Status().Update(ctx, r.resDef); err != nil {
		return ctrl.Result{}, err
	}

	return ctrl.Result{}, nil
}

// SetupWithManager specifies how the controller is built to watch a CR and
// other resources that are owned and managed by that controller
func (r *PublicKeyReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&workspacev1.PublicKey{}).
		Complete(r)
}

func (r *PublicKeyReconciler) updateResourceStatusOnError(err error) {
	r.resDef.Status.ReconcileSuccess = false
	r.resDef.Status.Message = fmt.Sprintf("Error:\t%s", err.Error())
	_ = r.Client.Status().Update(context.Background(), r.resDef)
}
