package controllers

import (
	"context"
	"fmt"

	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"

	corev1 "k8s.io/api/core/v1"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func (r *SupportServiceReconciler) buildPVC(
	name string,
	spec corev1.PersistentVolumeClaimSpec,
) *corev1.PersistentVolumeClaim {

	labels := map[string]string{
		"app": "workspace-supportservice",
	}

	pvc := &corev1.PersistentVolumeClaim{
		ObjectMeta: metav1.ObjectMeta{
			Name:      fmt.Sprintf("supportservice-%s", name),
			Namespace: r.resDef.Namespace,
			Labels:    labels,
		},
		Spec: spec,
	}

	return pvc
}

func (r *SupportServiceReconciler) createPVC() error {
	ctx := context.Background()

	for _, sharevolume := range r.resDef.Spec.SharedVolumes {
		// build PVC
		pvcDef := r.buildPVC(sharevolume.Name, sharevolume.PVC)

		// add ownership
		if err := controllerutil.SetControllerReference(
			r.resDef,
			pvcDef,
			r.Scheme,
		); err != nil {
			return err
		}

		// get existing PVC
		pvc := &corev1.PersistentVolumeClaim{}
		err := r.Client.Get(
			ctx,
			client.ObjectKey{
				Namespace: pvcDef.Namespace,
				Name:      pvcDef.Name,
			},
			pvc,
		)

		// create if not found
		if apierrors.IsNotFound(err) {
			r.log.Info("PVC is not found, creating one")
			if err := r.Client.Create(ctx, pvcDef); err != nil {
				r.log.Error(err, fmt.Sprintf("Failed to create PVC %s", pvc.Name))
				return err
			}

		} else if err != nil {
			r.log.Error(err, "Failed to gather PVC info")
			return err
		}

	}

	return nil
}
