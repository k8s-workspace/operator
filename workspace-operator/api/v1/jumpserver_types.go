/*


Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1

import (
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// SSHServer config for ssh-server container
type SSHServer struct {
	// +kubebuilder:validation:Optional
	// +kubebuilder:default={limits: {cpu: "100m", memory: "128M"}, requests: {cpu: "100m", memory: "128M"}}
	Resources corev1.ResourceRequirements `json:"resources,omitempty"`

	// +kubebuilder:validation:Optional
	// +kubebuilder:default=2222
	ServicePort int32 `json:"servicePort,omitempty"`

	// +kubebuilder:validation:Optional
	// +kubebuilder:default="LoadBalancer"
	ServiceType *corev1.ServiceType `json:"serviceType,omitempty"`

	// +kubebuilder:validation:Optional
	// +kubebuilder:default=""
	SSHdConfig string `json:"sshdConfig,omitempty"`
}

// AuthorizedKeysReload config for auth-key reload container
type AuthorizedKeysReload struct {
	// +kubebuilder:validation:Optional
	// +kubebuilder:default={requests: {cpu: "10m", memory: "128M"}, limits: {cpu: "50m", memory: "128M"}}
	Resources corev1.ResourceRequirements `json:"resources,omitempty"`
}

// JumpServerSpec defines the desired state of JumpServer
type JumpServerSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "make" to regenerate code after modifying this file

	// +kubebuilder:validation:Optional
	// +kubebuilder:validation:Type=integer
	// +kubebuilder:default=1
	Size int32 `json:"size"`

	// +kubebuilder:validation:Optional
	// +kubebuilder:default={}
	SSHServer SSHServer `json:"sshServer,omitempty"`

	// +kubebuilder:validation:Optional
	// +kubebuilder:default={resources:{requests: {cpu: "10m", memory: "128M"}, limits: {cpu: "50m", memory: "128M"}}}
	AuthorizedKeysReload *AuthorizedKeysReload `json:"authorizedKeysReload,omitempty"`
}

// JumpServerStatus defines the observed state of JumpServer
type JumpServerStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file

	Deployment           corev1.ObjectReference `json:"deployment,omitempty"`
	AuthorizedKeysSecret corev1.ObjectReference `json:"authorizedKeysSecret,omitempty"`
	Service              corev1.ObjectReference `json:"secret,omitempty"`
	SSHdConfig           corev1.ObjectReference `json:"sshdConfig,omitempty"`

	ReconcileSuccess bool   `json:"reconcileSuccess,omitempty"`
	Message          string `json:"message,omitempty"`
}

// +kubebuilder:object:root=true
// +kubebuilder:subresource:status

// JumpServer is the Schema for the jumpservers API
// +kubebuilder:subresource:status
type JumpServer struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	// +kubebuilder:default={size:1,authorizedKeysReload:{resources:{requests:{cpu:"100m",memory:"128M"},limits:{cpu:"100m",memory:"128M"},}},sshServer:{resources:{requests:{cpu:"100m",memory:"128M"},limits:{cpu:"100m",memory:"128M"}}}}
	Spec   JumpServerSpec   `json:"spec,omitempty"`
	Status JumpServerStatus `json:"status,omitempty"`
}

// +kubebuilder:object:root=true

// JumpServerList contains a list of JumpServer
type JumpServerList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []JumpServer `json:"items"`
}

func init() {
	SchemeBuilder.Register(&JumpServer{}, &JumpServerList{})
}
