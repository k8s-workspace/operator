/*


Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1

import (
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// ContainerMount defines which container and where a volume is supposed to
// be mounted
type ContainerMount struct {
	Path      string `json:"path"`
	Container string `json:"container"`
}

// SupportContainer defines a container for supporting a service
type SupportContainer struct {
	Spec corev1.Container `json:"spec"`
	// +kubebuilder:validation:Optional
	// +kubebuilder:default=false
	UseHelperCmdPreventingCrashLoop bool `json:"useHelperCmdPreventingCrashLoop,omitempty"`
}

// SharedVolume defines volume shareed between the workspace and the support
// container
type SharedVolume struct {
	// Name of the volume
	Name string `json:"name,omitempty"`
	// Container mount specs
	ContainerMounts []ContainerMount `json:"containerMounts"`

	// WorkspacePath defines where the volume to be mounted in the workspace
	// the volume will not be mounted to the workspace if this fields is ommitted
	// or empty
	// +kubebuilder:validation:Optional
	// +kubebuilder:default=""
	WorkspacePath string `json:"workspacePath,omitempty"`

	// PVC defines the persistent volume claim specs
	PVC corev1.PersistentVolumeClaimSpec `json:"pvc"`
}

// HomeVolumeMount defines the the MountPath and the SubPath for mounting
// homeVolume to SupportServices
type HomeVolumeMount struct {
	// +kubebuilder:default=/workspace-home
	MountPath string `json:"mountPath"`
	// +kubebuilder:validation:Optional
	SubPath string `json:"subPath,omitempty"`
}

// SupportServiceSpec defines the desired state of SupportService
type SupportServiceSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "make" to regenerate code after modifying this file

	// SharedVolumes defines a list of shared volumes between containers
	// +kubebuilder:validation:Optional
	SharedVolumes []SharedVolume `json:"sharedVolumes,omitempty"`

	// Containers defines a list of support containers
	Containers []SupportContainer `json:"containers"`

	ImagePullSecrets []corev1.LocalObjectReference `json:"imagePullSecrets,omitempty"`

	ServicePorts []corev1.ServicePort `json:"servicePorts,omitempty"`

	// +kubebuilder:validation:Optional
	// +kubebuilder:default={mountPath: "/workspace-home"}
	HomeVolumeMount HomeVolumeMount `json:"homeVolumeMount,omitempty"`

	// For defines the name of the workspace this SupportService is for
	For string `json:"for"`
}

// SupportServiceStatus defines the observed state of SupportService
type SupportServiceStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file

	ReconcileSuccess bool   `json:"reconcileSuccess,omitempty"`
	Message          string `json:"message,omitempty"`

	AttachedTo corev1.ObjectReference `json:"attachedTo,omitempty"`
}

// +kubebuilder:object:root=true
// +kubebuilder:subresource:status

// SupportService is the Schema for the supportservices API
type SupportService struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   SupportServiceSpec   `json:"spec,omitempty"`
	Status SupportServiceStatus `json:"status,omitempty"`
}

// +kubebuilder:object:root=true

// SupportServiceList contains a list of SupportService
type SupportServiceList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []SupportService `json:"items"`
}

func init() {
	SchemeBuilder.Register(&SupportService{}, &SupportServiceList{})
}
