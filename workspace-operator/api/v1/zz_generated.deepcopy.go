// +build !ignore_autogenerated

/*


Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Code generated by controller-gen. DO NOT EDIT.

package v1

import (
	corev1 "k8s.io/api/core/v1"
	runtime "k8s.io/apimachinery/pkg/runtime"
)

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *AuthorizedKeysReload) DeepCopyInto(out *AuthorizedKeysReload) {
	*out = *in
	in.Resources.DeepCopyInto(&out.Resources)
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new AuthorizedKeysReload.
func (in *AuthorizedKeysReload) DeepCopy() *AuthorizedKeysReload {
	if in == nil {
		return nil
	}
	out := new(AuthorizedKeysReload)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *ContainerMount) DeepCopyInto(out *ContainerMount) {
	*out = *in
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new ContainerMount.
func (in *ContainerMount) DeepCopy() *ContainerMount {
	if in == nil {
		return nil
	}
	out := new(ContainerMount)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *HomeDir) DeepCopyInto(out *HomeDir) {
	*out = *in
	if in.PVC != nil {
		in, out := &in.PVC, &out.PVC
		*out = new(corev1.PersistentVolumeClaimSpec)
		(*in).DeepCopyInto(*out)
	}
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new HomeDir.
func (in *HomeDir) DeepCopy() *HomeDir {
	if in == nil {
		return nil
	}
	out := new(HomeDir)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *HomeVolumeMount) DeepCopyInto(out *HomeVolumeMount) {
	*out = *in
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new HomeVolumeMount.
func (in *HomeVolumeMount) DeepCopy() *HomeVolumeMount {
	if in == nil {
		return nil
	}
	out := new(HomeVolumeMount)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *JumpServer) DeepCopyInto(out *JumpServer) {
	*out = *in
	out.TypeMeta = in.TypeMeta
	in.ObjectMeta.DeepCopyInto(&out.ObjectMeta)
	in.Spec.DeepCopyInto(&out.Spec)
	out.Status = in.Status
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new JumpServer.
func (in *JumpServer) DeepCopy() *JumpServer {
	if in == nil {
		return nil
	}
	out := new(JumpServer)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyObject is an autogenerated deepcopy function, copying the receiver, creating a new runtime.Object.
func (in *JumpServer) DeepCopyObject() runtime.Object {
	if c := in.DeepCopy(); c != nil {
		return c
	}
	return nil
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *JumpServerList) DeepCopyInto(out *JumpServerList) {
	*out = *in
	out.TypeMeta = in.TypeMeta
	in.ListMeta.DeepCopyInto(&out.ListMeta)
	if in.Items != nil {
		in, out := &in.Items, &out.Items
		*out = make([]JumpServer, len(*in))
		for i := range *in {
			(*in)[i].DeepCopyInto(&(*out)[i])
		}
	}
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new JumpServerList.
func (in *JumpServerList) DeepCopy() *JumpServerList {
	if in == nil {
		return nil
	}
	out := new(JumpServerList)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyObject is an autogenerated deepcopy function, copying the receiver, creating a new runtime.Object.
func (in *JumpServerList) DeepCopyObject() runtime.Object {
	if c := in.DeepCopy(); c != nil {
		return c
	}
	return nil
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *JumpServerSpec) DeepCopyInto(out *JumpServerSpec) {
	*out = *in
	in.SSHServer.DeepCopyInto(&out.SSHServer)
	if in.AuthorizedKeysReload != nil {
		in, out := &in.AuthorizedKeysReload, &out.AuthorizedKeysReload
		*out = new(AuthorizedKeysReload)
		(*in).DeepCopyInto(*out)
	}
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new JumpServerSpec.
func (in *JumpServerSpec) DeepCopy() *JumpServerSpec {
	if in == nil {
		return nil
	}
	out := new(JumpServerSpec)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *JumpServerStatus) DeepCopyInto(out *JumpServerStatus) {
	*out = *in
	out.Deployment = in.Deployment
	out.AuthorizedKeysSecret = in.AuthorizedKeysSecret
	out.Service = in.Service
	out.SSHdConfig = in.SSHdConfig
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new JumpServerStatus.
func (in *JumpServerStatus) DeepCopy() *JumpServerStatus {
	if in == nil {
		return nil
	}
	out := new(JumpServerStatus)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *PublicKey) DeepCopyInto(out *PublicKey) {
	*out = *in
	out.TypeMeta = in.TypeMeta
	in.ObjectMeta.DeepCopyInto(&out.ObjectMeta)
	out.Spec = in.Spec
	out.Status = in.Status
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new PublicKey.
func (in *PublicKey) DeepCopy() *PublicKey {
	if in == nil {
		return nil
	}
	out := new(PublicKey)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyObject is an autogenerated deepcopy function, copying the receiver, creating a new runtime.Object.
func (in *PublicKey) DeepCopyObject() runtime.Object {
	if c := in.DeepCopy(); c != nil {
		return c
	}
	return nil
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *PublicKeyList) DeepCopyInto(out *PublicKeyList) {
	*out = *in
	out.TypeMeta = in.TypeMeta
	in.ListMeta.DeepCopyInto(&out.ListMeta)
	if in.Items != nil {
		in, out := &in.Items, &out.Items
		*out = make([]PublicKey, len(*in))
		for i := range *in {
			(*in)[i].DeepCopyInto(&(*out)[i])
		}
	}
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new PublicKeyList.
func (in *PublicKeyList) DeepCopy() *PublicKeyList {
	if in == nil {
		return nil
	}
	out := new(PublicKeyList)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyObject is an autogenerated deepcopy function, copying the receiver, creating a new runtime.Object.
func (in *PublicKeyList) DeepCopyObject() runtime.Object {
	if c := in.DeepCopy(); c != nil {
		return c
	}
	return nil
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *PublicKeySpec) DeepCopyInto(out *PublicKeySpec) {
	*out = *in
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new PublicKeySpec.
func (in *PublicKeySpec) DeepCopy() *PublicKeySpec {
	if in == nil {
		return nil
	}
	out := new(PublicKeySpec)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *PublicKeyStatus) DeepCopyInto(out *PublicKeyStatus) {
	*out = *in
	out.AssociatedSecret = in.AssociatedSecret
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new PublicKeyStatus.
func (in *PublicKeyStatus) DeepCopy() *PublicKeyStatus {
	if in == nil {
		return nil
	}
	out := new(PublicKeyStatus)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *SSHServer) DeepCopyInto(out *SSHServer) {
	*out = *in
	in.Resources.DeepCopyInto(&out.Resources)
	if in.ServiceType != nil {
		in, out := &in.ServiceType, &out.ServiceType
		*out = new(corev1.ServiceType)
		**out = **in
	}
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new SSHServer.
func (in *SSHServer) DeepCopy() *SSHServer {
	if in == nil {
		return nil
	}
	out := new(SSHServer)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *SharedVolume) DeepCopyInto(out *SharedVolume) {
	*out = *in
	if in.ContainerMounts != nil {
		in, out := &in.ContainerMounts, &out.ContainerMounts
		*out = make([]ContainerMount, len(*in))
		copy(*out, *in)
	}
	in.PVC.DeepCopyInto(&out.PVC)
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new SharedVolume.
func (in *SharedVolume) DeepCopy() *SharedVolume {
	if in == nil {
		return nil
	}
	out := new(SharedVolume)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *SupportContainer) DeepCopyInto(out *SupportContainer) {
	*out = *in
	in.Spec.DeepCopyInto(&out.Spec)
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new SupportContainer.
func (in *SupportContainer) DeepCopy() *SupportContainer {
	if in == nil {
		return nil
	}
	out := new(SupportContainer)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *SupportService) DeepCopyInto(out *SupportService) {
	*out = *in
	out.TypeMeta = in.TypeMeta
	in.ObjectMeta.DeepCopyInto(&out.ObjectMeta)
	in.Spec.DeepCopyInto(&out.Spec)
	out.Status = in.Status
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new SupportService.
func (in *SupportService) DeepCopy() *SupportService {
	if in == nil {
		return nil
	}
	out := new(SupportService)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyObject is an autogenerated deepcopy function, copying the receiver, creating a new runtime.Object.
func (in *SupportService) DeepCopyObject() runtime.Object {
	if c := in.DeepCopy(); c != nil {
		return c
	}
	return nil
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *SupportServiceList) DeepCopyInto(out *SupportServiceList) {
	*out = *in
	out.TypeMeta = in.TypeMeta
	in.ListMeta.DeepCopyInto(&out.ListMeta)
	if in.Items != nil {
		in, out := &in.Items, &out.Items
		*out = make([]SupportService, len(*in))
		for i := range *in {
			(*in)[i].DeepCopyInto(&(*out)[i])
		}
	}
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new SupportServiceList.
func (in *SupportServiceList) DeepCopy() *SupportServiceList {
	if in == nil {
		return nil
	}
	out := new(SupportServiceList)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyObject is an autogenerated deepcopy function, copying the receiver, creating a new runtime.Object.
func (in *SupportServiceList) DeepCopyObject() runtime.Object {
	if c := in.DeepCopy(); c != nil {
		return c
	}
	return nil
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *SupportServiceSpec) DeepCopyInto(out *SupportServiceSpec) {
	*out = *in
	if in.SharedVolumes != nil {
		in, out := &in.SharedVolumes, &out.SharedVolumes
		*out = make([]SharedVolume, len(*in))
		for i := range *in {
			(*in)[i].DeepCopyInto(&(*out)[i])
		}
	}
	if in.Containers != nil {
		in, out := &in.Containers, &out.Containers
		*out = make([]SupportContainer, len(*in))
		for i := range *in {
			(*in)[i].DeepCopyInto(&(*out)[i])
		}
	}
	if in.ImagePullSecrets != nil {
		in, out := &in.ImagePullSecrets, &out.ImagePullSecrets
		*out = make([]corev1.LocalObjectReference, len(*in))
		copy(*out, *in)
	}
	if in.ServicePorts != nil {
		in, out := &in.ServicePorts, &out.ServicePorts
		*out = make([]corev1.ServicePort, len(*in))
		for i := range *in {
			(*in)[i].DeepCopyInto(&(*out)[i])
		}
	}
	out.HomeVolumeMount = in.HomeVolumeMount
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new SupportServiceSpec.
func (in *SupportServiceSpec) DeepCopy() *SupportServiceSpec {
	if in == nil {
		return nil
	}
	out := new(SupportServiceSpec)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *SupportServiceStatus) DeepCopyInto(out *SupportServiceStatus) {
	*out = *in
	out.AttachedTo = in.AttachedTo
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new SupportServiceStatus.
func (in *SupportServiceStatus) DeepCopy() *SupportServiceStatus {
	if in == nil {
		return nil
	}
	out := new(SupportServiceStatus)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *User) DeepCopyInto(out *User) {
	*out = *in
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new User.
func (in *User) DeepCopy() *User {
	if in == nil {
		return nil
	}
	out := new(User)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *Workspace) DeepCopyInto(out *Workspace) {
	*out = *in
	out.TypeMeta = in.TypeMeta
	in.ObjectMeta.DeepCopyInto(&out.ObjectMeta)
	in.Spec.DeepCopyInto(&out.Spec)
	out.Status = in.Status
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new Workspace.
func (in *Workspace) DeepCopy() *Workspace {
	if in == nil {
		return nil
	}
	out := new(Workspace)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyObject is an autogenerated deepcopy function, copying the receiver, creating a new runtime.Object.
func (in *Workspace) DeepCopyObject() runtime.Object {
	if c := in.DeepCopy(); c != nil {
		return c
	}
	return nil
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *WorkspaceList) DeepCopyInto(out *WorkspaceList) {
	*out = *in
	out.TypeMeta = in.TypeMeta
	in.ListMeta.DeepCopyInto(&out.ListMeta)
	if in.Items != nil {
		in, out := &in.Items, &out.Items
		*out = make([]Workspace, len(*in))
		for i := range *in {
			(*in)[i].DeepCopyInto(&(*out)[i])
		}
	}
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new WorkspaceList.
func (in *WorkspaceList) DeepCopy() *WorkspaceList {
	if in == nil {
		return nil
	}
	out := new(WorkspaceList)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyObject is an autogenerated deepcopy function, copying the receiver, creating a new runtime.Object.
func (in *WorkspaceList) DeepCopyObject() runtime.Object {
	if c := in.DeepCopy(); c != nil {
		return c
	}
	return nil
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *WorkspaceSpec) DeepCopyInto(out *WorkspaceSpec) {
	*out = *in
	if in.Home != nil {
		in, out := &in.Home, &out.Home
		*out = new(HomeDir)
		(*in).DeepCopyInto(*out)
	}
	if in.User != nil {
		in, out := &in.User, &out.User
		*out = new(User)
		**out = **in
	}
	if in.Container != nil {
		in, out := &in.Container, &out.Container
		*out = new(corev1.Container)
		(*in).DeepCopyInto(*out)
	}
	if in.AuthorizedKeysReload != nil {
		in, out := &in.AuthorizedKeysReload, &out.AuthorizedKeysReload
		*out = new(AuthorizedKeysReload)
		(*in).DeepCopyInto(*out)
	}
	if in.ServicePorts != nil {
		in, out := &in.ServicePorts, &out.ServicePorts
		*out = make([]corev1.ServicePort, len(*in))
		for i := range *in {
			(*in)[i].DeepCopyInto(&(*out)[i])
		}
	}
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new WorkspaceSpec.
func (in *WorkspaceSpec) DeepCopy() *WorkspaceSpec {
	if in == nil {
		return nil
	}
	out := new(WorkspaceSpec)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *WorkspaceStatus) DeepCopyInto(out *WorkspaceStatus) {
	*out = *in
	out.Deployment = in.Deployment
	out.Service = in.Service
	out.PasswordSecret = in.PasswordSecret
	out.AuthorizedKeysSecret = in.AuthorizedKeysSecret
	out.SSHdConfig = in.SSHdConfig
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new WorkspaceStatus.
func (in *WorkspaceStatus) DeepCopy() *WorkspaceStatus {
	if in == nil {
		return nil
	}
	out := new(WorkspaceStatus)
	in.DeepCopyInto(out)
	return out
}
