/*


Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1

import (
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// HomeDir specifies configurations for workspace's home directory
type HomeDir struct {
	// +kubebuilder:validation:Optional
	// +kubebuilder:default={accessModes: {"ReadWriteOnce"}, resources: {requests: {storage: "10Gi"}}}
	PVC *corev1.PersistentVolumeClaimSpec `json:"pvc,omitempty"`
}

// User specifies user configurations for the workspace's user
type User struct {
	// +kubebuilder:validation:Optional
	// +kubebuilder:default=1001
	UID int16 `json:"uid"`

	// +kubebuilder:validation:Optional
	// +kubebuilder:default=1001
	GID int16 `json:"gid"`

	// +kubebuilder:validation:Optional
	// +kubebuilder:default="workspace-default"
	Username string `json:"username"`

	// +kubebuilder:validation:Optional
	// +kubebuilder:default="workspace-default-password"
	PasswordSecretName string `json:"passwordSecretName"`
}

// WorkspaceSpec defines the desired state of Workspace
type WorkspaceSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "make" to regenerate code after modifying this file

	// +kubebuilder:validation:Optional
	// +kubebuilder:default={pvc:{accessModes: {"ReadWriteOnce"}, resources: {requests: {storage: "10Gi"}}}}
	Home *HomeDir `json:"home,omitempty"`

	// +kubebuilder:validation:Optional
	// +kubebuilder:default={uid: 1001, gid: 1001, username: "workspace-default", passwordSecretName: "workspace-default-password"}
	User *User `json:"user,omitempty"`

	Container *corev1.Container `json:"container,omitempty"`

	// +kubebuilder:validation:Optional
	// +kubebuilder:default={resources:{requests: {cpu: "10m", memory: "128M"}, limits: {cpu: "50m", memory: "128M"}}}
	AuthorizedKeysReload *AuthorizedKeysReload `json:"authorizedKeysReload,omitempty"`

	// +kubebuilder:validation:Optional
	// +kubebuilder:default=""
	SSHdConfig string `json:"sshdConfig,omitempty"`

	// List of ports to be exposed in addition to the ssh port
	// +kubebuilder:validation:Optional
	// +kubebuilder:default={}
	ServicePorts []corev1.ServicePort `json:"servicePorts,omitempty"`
	// +kubebuilder:validation:Optional
	// +kubebuilder:default="ClusterIP"
	ServiceType corev1.ServiceType `json:"serviceType,omitempty"`
}

// WorkspaceStatus defines the observed state of Workspace
type WorkspaceStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file

	Deployment           corev1.ObjectReference `json:"deployment"`
	Service              corev1.ObjectReference `json:"service"`
	PasswordSecret       corev1.ObjectReference `json:"passwordSecret"`
	AuthorizedKeysSecret corev1.ObjectReference `json:"authorizedKeysSecret"`
	SSHdConfig           corev1.ObjectReference `json:"sshdConfig,omitempty"`

	ReconcileSuccess bool   `json:"reconcileSuccess,omitempty"`
	Message          string `json:"message,omitempty"`
}

// +kubebuilder:object:root=true
// +kubebuilder:subresource:status

// Workspace is the Schema for the workspaces API
type Workspace struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   WorkspaceSpec   `json:"spec,omitempty"`
	Status WorkspaceStatus `json:"status,omitempty"`
}

// +kubebuilder:object:root=true

// WorkspaceList contains a list of Workspace
type WorkspaceList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []Workspace `json:"items"`
}

func init() {
	SchemeBuilder.Register(&Workspace{}, &WorkspaceList{})
}
