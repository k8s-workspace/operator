# Kubernetes Workspace

## Overview

![overview](./diagrams/overview.svg)

## Usage

### Deploy Operator

```console
$ curl https://k8s-workspace.gitlab.io/operator/bundle.yaml | kubectl apply -f -
...
namespace/workspace-system created
customresourcedefinition.apiextensions.k8s.io/jumpservers.workspace.bailinhe.com configured
customresourcedefinition.apiextensions.k8s.io/publickeys.workspace.bailinhe.com configured
customresourcedefinition.apiextensions.k8s.io/supportservices.workspace.bailinhe.com configured
customresourcedefinition.apiextensions.k8s.io/workspaces.workspace.bailinhe.com configured
...
```

### Deploy JumpServer

- jumpserver.yaml

  ```yaml
  apiVersion: workspace.bailinhe.com/v1
  kind: JumpServer
  metadata:
    name: default
    namespace: workspace-system
  spec:
    sshServer:
      servicePort: 2222
      serviceType: LoadBalancer

    # Optionally sshd config can be provided, the following file contains
    # the default config.
    # Note that the jumpserver WILL FAILED IF
    #  - Port is modified
    #  - HostKey is modified
    #  - AuthorizedKeysFile is modified
    #  - PubkeyAuthentication set to be no
    sshdConfig: |
      Port 22

      HostKey /etc/ssh/ssh_host_rsa_key
      HostKey /etc/ssh/ssh_host_dsa_key
      HostKey /etc/ssh/ssh_host_ecdsa_key
      HostKey /etc/ssh/ssh_host_ed25519_key

      SyslogFacility AUTH
      LogLevel INFO

      LoginGraceTime 2m
      PermitRootLogin no
      StrictModes yes
      MaxAuthTries 6
      MaxSessions 10

      PubkeyAuthentication yes
      PasswordAuthentication no
      PermitEmptyPasswords no
      AuthorizedKeysFile      .ssh/authorized_keys
      ChallengeResponseAuthentication no
      UsePAM yes
      X11Forwarding no
      PrintMotd no
  ```

- deploy

  ```console
  $ kubectl apply -f jumpserver.yaml
  jumpserver.workspace.bailinhe.com/default created
  ```

### Create PublicKey

- publickey.yaml

  ```yaml
  apiVersion: workspace.bailinhe.com/v1
  kind: PublicKey
  metadata:
    name: default
    namespace: my-namespace
  spec:
    keyData: |
      ssh-rsa SomeKeyDataSomeKeyDataomeKeyDataomeKeyData user@somehost
  ```

- create key

  ```console
  $ kubectl apply -f publickey.yaml
  publickey.workspace.bailinhe.com/default created
  ```

### Create Workspace

- workspace.yaml

  ```yaml
  apiVersion: workspace.bailinhe.com/v1
  kind: Workspace
  metadata:
    name: default
    namespace: my-namespace
  spec:
    home:
      pvc:
        storageClassName: standard
        accessModes:
          - ReadWriteOnce
        resources:
          requests:
            storage: 10Gi

    container:
      name: workspace
      image: registry.gitlab.com/k8s-workspace/images/workspace/ubuntu:latest
      imagePullPolicy: Always
      resources:
        limits:
          cpu: 500m
          memory: 1024M
        requests:
          cpu: 100m
          memory: 256M
      ports:
        - containerPort: 3000
          protocol: TCP
        
    user:
      uid: 1001
      gid: 1001
      username: workspace-default

      # password secret will be generated if `passwordSecretName` is not defined
      # or secret with the given name is not exist, note that this secret will
      # be mounted at /secret in the workspace
      passwordSecretName: workspace-default-password

    # some ports can be exposed IN ADDITION TO port 22 for ssh
    servicePorts:
      - name: node
        targetPort: 3000
        port: 3000


    # optionally sshd config can be provided
    sshdConfig: |
      Port 22
      ...
  ```

- create workspace

  ```console
  $ kubectl apply -f workspace.yaml
  workspace.workspace.bailinhe.com/default created
  ```

- check workspace status

  ```console
  $ kubectl get workspace -n my-namespace
  NAME      AGE
  default   96s

  $ kubectl describe workspace default -n my-namespace
  ...
  Status:
  Authorized Keys Secret:
    API Version:       v1
    Kind:              Secret
    Name:              workspace-authorized-keys
    Namespace:         bhdev
    Resource Version:  38158213
    UID:               cdc9263d-4490-470c-a0aa-a943c26d1fd7
  Deployment:
    API Version:       apps/v1
    Kind:              Deployment
    Name:              default-workspace
    Namespace:         bhdev
    Resource Version:  38158223
    UID:               de925b44-ff87-4193-bb2c-8390518b9fc5
  Message:             Success
  Password Secret:
    API Version:       v1
    Kind:              Secret
    Name:              workspace-default-password
    Namespace:         bhdev
    Resource Version:  38158216
    UID:               96cdc11e-b6bf-4c4c-bce1-4c5aa2407d1c
  Reconcile Success:   true
  Service:
    API Version:       v1
    Kind:              Service
    Name:              default-workspace
    Namespace:         bhdev
    Resource Version:  38158229
    UID:               be43cb2f-382b-4806-b466-73793902f969
  Sshd Config:
    API Version:       v1
    Kind:              ConfigMap
    Name:              default-workspace-sshd-config
    Namespace:         bhdev
    Resource Version:  38158217
    UID:               3ce4c7d0-2d4b-491e-8e1a-effa512cf77c
  ...
  ```

### Create SupportService

`SupportService` is a container or a list of containers to provide additional
service to the workspace (e.g. `MySQL`, `Java`, `NGINX`, etc),
running as sidecar(s) to the main `Workspace`,
with volumes shared between the containers,
see [SupportService](#supportservice).

- supportservice.yaml

  ```yaml
  apiVersion: workspace.bailinhe.com/v1
  kind: SupportService
  metadata:
    name: sample
  spec:
    for: sample

    # sharedVolumes:
    #   - name: node
    #     containerMounts:
    #       - path: /some-graphql-project
    #         container: gql
    #     workspacePath: /home/workspace-default/some-graphql-project
    #     pvc:
    #       accessModes:
    #         - ReadWriteOnce
    #       resources:
    #         requests:
    #           storage: 1Gi
    #       storageClassName: standard

    homeVolumeMount:
      mountPath: /app
      subPath: some-graphql-project

    containers:
      - spec:
          name: gql
          image: node:latest
          imagePullPolicy: IfNotPresent
          resources:
            requests:
              cpu: 100m
              memory: 100M
            limits:
              cpu: 100m
              memory: 100M
          command:
            - /bin/sh
          args: []
            - -c
            - |
              cd /some-graphql-project
              npm i
              npm run dev
          env: []
          ports:
            - containerPort: 5000
              protocol: TCP

        # If true, command and args will be overwritten by a sleep loop, this is
        # useful when the shared volume is not ready (i.e., the `npm` commandsw
        # fail because the `/some-graphql-project` is empty upon creation)
        useHelperCmdPreventingCrashLoop: true

    imagePullSecrets: []

    servicePorts:
      - name: gql
        port: 5000
        targetPort: 5000
        protocol: TCP
  ```

- create supportservice

  ```console
  $ kubectl apply -f supportservice.yaml
  supportservice.workspace.bailinhe.com/default created 
  ```

## Implementation

### JumpServer

![jumpserver](./diagrams/jumpserver.svg)

### Workspace

![workspace](./diagrams/workspace.svg)

### PublicKey

![publickey](./diagrams/publickey.svg)

### SupportService

![supportservice](./diagrams/supportservice.svg)
